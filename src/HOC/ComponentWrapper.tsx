import React, {PureComponent} from "react";
import {toast, ToastContainer} from "react-toastify";
import {connect} from 'react-redux';
import {Dispatch} from "redux";

interface Props {
    children: JSX.Element,
    errorMessage: String,
    dispatch: Dispatch,
}

class ComponentWrapper extends PureComponent<Props> {
    componentDidUpdate(prevProps: Readonly<Props>, prevState: Readonly<{}>, snapshot?: any) {
        const {errorMessage, dispatch} = this.props;
        if(errorMessage && prevProps.errorMessage !== errorMessage) {
            toast(errorMessage, {
                onClose: () => dispatch({type: 'RESET_ALL_ACTIVITY_ERRORS'})
            });
        }
    }

    render() {
        const {children} = this.props;

        return <>
            {children}
            <ToastContainer
                position='top-center'
                autoClose={3000}
                closeOnClick
            />
        </>
    }
}

export default connect()(ComponentWrapper);
