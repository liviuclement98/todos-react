import React from "react";
import {useOktaAuth} from "@okta/okta-react";
import {AuthState, OktaAuth} from "@okta/okta-auth-js";

export interface OktaProps<Props = any, State = any> {
    oktaAuth?: OktaAuth;
    authState?: AuthState;
}

export function withOkta<P extends OktaProps>( Child: React.ComponentClass<P> | React.ComponentType<P> ) {
    return ( props: Omit<P, keyof OktaProps> ) => {
        const {oktaAuth,authState } = useOktaAuth();

        return <Child { ...props as P } authState={ authState } oktaAuth={ oktaAuth } />;
    }
}
