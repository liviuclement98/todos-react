import {Navigate, useLocation} from "react-router-dom";
import {withOktaAuth} from "@okta/okta-react";
import {AuthState} from "@okta/okta-auth-js";

const RequireLoggedIn = ({authState, children }: { authState: AuthState, children: JSX.Element }) => {
    let location = useLocation();

    if (!authState?.isAuthenticated) {
        return <Navigate to="/" state={{ from: location }} replace />;
    }

    return children;
}

export default withOktaAuth(RequireLoggedIn as any)
