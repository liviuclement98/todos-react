import {Navigate, useLocation} from "react-router-dom";
import {AuthState} from "@okta/okta-auth-js";
import {withOktaAuth} from "@okta/okta-react";

const RequireLoggedOut = ({authState, children }: { authState: AuthState, children: JSX.Element }) => {
    let location = useLocation();

    if (authState?.isAuthenticated) {
        return <Navigate to="/home" state={{ from: location }} replace />;
    }

    return children;
}

export default withOktaAuth(RequireLoggedOut as any);
