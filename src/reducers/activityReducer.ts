export default function activityReducer(state = { ...defaultState, }, action: any) {
    let newState = { ...state, };
    switch(action.type) {
        case 'ON_GET_ALL_ACTIVITIES': {
            newState.fetchingAllActivities = true;
            newState.fetchingAllActivitiesError = '';
            return newState;
        }
        case 'ON_GET_ALL_ACTIVITIES_FULFILLED': {
            const { activityArray, } = action.payload;

            newState.fetchingAllActivities = false;
            newState.fetchedAllActivities = true;
            // @ts-ignore
            newState.activityArray = activityArray;
            return newState;
        }
        case 'ON_GET_ALL_ACTIVITIES_REJECTED': {
            const { error, } = action.payload;
            newState.fetchingAllActivities = false;
            newState.fetchedAllActivities = false;
            newState.fetchingAllActivitiesError = error;
            return newState;
        }
        case 'ON_ADD_NEW_ACTIVITY': {
            newState.addingNewActivity = true;
            newState.addingNewActivityError = '';
            return newState;
        }
        case 'ON_ADD_NEW_ACTIVITY_FULFILLED': {
            const { newActivity, } = action.payload;
            const newActivityArray = [...newState.activityArray, newActivity];

            newState.addingNewActivity = false;
            newState.addedNewActivity = true;
            // @ts-ignore
            newState.activityArray = newActivityArray;
            return newState;
        }
        case 'ON_ADD_NEW_ACTIVITY_REJECTED': {
            const { error, } = action.payload;
            newState.addingNewActivity = false;
            newState.addedNewActivity = false;
            newState.addingNewActivityError = error;
            return newState;
        }
        case 'ON_EDIT_ACTIVITY': {
            newState.editingActivity = true;
            newState.editingActivityError = '';
            return newState;
        }
        case 'ON_EDIT_ACTIVITY_FULFILLED': {
            const { editedActivity, } = action.payload;
            const newActivityArray = [...newState.activityArray];
            let activityToEditIndex = newActivityArray.findIndex((activity: any) => activity.id === editedActivity.id)

            // @ts-ignore
            newActivityArray[activityToEditIndex] = {...editedActivity}

            newState.editingActivity = false;
            newState.editedActivity = true;
            newState.activityArray = newActivityArray;
            return newState;
        }
        case 'ON_EDIT_ACTIVITY_REJECTED': {
            const { error, } = action.payload;
            newState.editingActivity = false;
            newState.editingActivityError = error;
            return newState;
        }
        case 'ON_DELETE_ACTIVITY': {
            newState.deletingActivity = true;
            newState.deletingActivityError = '';
            return newState;
        }
        case 'ON_DELETE_ACTIVITY_FULFILLED': {
            const { deletedActivityId, } = action.payload;
            const newActivityArray = newState.activityArray.filter((activity: any) => activity.id !== deletedActivityId);

            newState.deletingActivity = false;
            newState.deletedActivity = true;
            newState.activityArray = newActivityArray;
            return newState;
        }
        case 'ON_DELETE_ACTIVITY_REJECTED': {
            const { error, } = action.payload;
            newState.deletingActivity = false;
            newState.deletingActivityError = error;
            return newState;
        }
        case 'RESET_ALL_ACTIVITY_ERRORS': {
            newState.fetchingAllActivitiesError = '';
            newState.addingNewActivityError = '';
            newState.editingActivityError = '';
            newState.deletingActivityError = '';
            return newState;
        }
        case 'RESET_EVERYTHING': {
            return { ...defaultState, };
        }
        default:
            return state;
    }
}

const defaultState = {
    activityArray: [],
    fetchingAllActivities: false,
    fetchingAllActivitiesError: '',
    fetchedAllActivities:false,
    addingNewActivity: false,
    addingNewActivityError: '',
    addedNewActivity: false,
    editingActivity: false,
    editingActivityError: '',
    editedActivity: false,
    deletingActivity: false,
    deletingActivityError: '',
    deletedActivity: false,
};

export interface ActivityState {
    activityArray: Array<any>,
    fetchingAllActivities: boolean,
    fetchingAllActivitiesError: string,
    fetchedAllActivities:boolean,
    addingNewActivity: boolean,
    addingNewActivityError: string,
    addedNewActivity: boolean,
    editingActivity: boolean,
    editingActivityError: string,
    editedActivity: boolean
    deletingActivity: boolean,
    deletingActivityError: string,
    deletedActivity: boolean
}
