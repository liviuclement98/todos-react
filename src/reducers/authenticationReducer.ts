export default function authenticationReducer(state = { ...defaultState, }, action: any) {
    let newState = { ...state, };
    switch(action.type) {
        case 'ON_LOGIN': {
            newState.loggingIn = true;
            newState.loggingInError = '';
            return newState;
        }
        case 'ON_LOGIN_FULFILLED': {
            const { user, token, } = action.payload;
            newState.loggingIn = false;
            newState.isLoggedIn = true;
            newState.user = user;
            newState.token = token;
            return newState;
        }
        case 'ON_LOGIN_REJECTED': {
            const { error, } = action.payload;
            newState.loggingIn = false;
            newState.loggingInError = error;
            return newState;
        }
        case 'ON_SIGNUP': {
            newState.signingUp = true;
            newState.signingUpError = '';
            return newState;
        }
        case 'ON_SIGNUP_FULFILLED': {
            newState.signingUp = false;
            newState.isSignedUp = true;
            return newState;
        }
        case 'ON_SIGNUP_REJECTED': {
            const { error, } = action.payload;
            newState.signingUp = false;
            newState.signingUpError = error;
            return newState;
        }
        case 'RESET_LOGIN': {
            newState.loggingIn = false;
            newState.loggingInError = '';
            return newState;
        }
        case 'ON_LOG_OUT': {
            newState.token = '';
            newState.user = {};
            newState.isLoggedIn = false;
            return newState;
        }
        case 'RESET_EVERYTHING': {
            return { ...defaultState, };
        }
        default:
            return state;
    }
}

const defaultState = {
    token: '',
    user: {},
    isLoggedIn: true,
    loggingIn: false,
    loggingInError: '',
    signingUp: false,
    signingUpError: '',
    isSignedUp: false,
};

export interface AuthenticationState {
    token: string,
    user: {
        email: string;
        password: string;
    };
    isLoggedIn: boolean,
    loggingIn: boolean,
    loggingInError: string,
    signingUp: boolean,
    signingUpError: string,
    isSignedUp: boolean,
}
