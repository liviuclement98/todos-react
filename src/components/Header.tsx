import React, {PureComponent} from "react";
import {connect} from 'react-redux'
import {Link} from "react-router-dom";
import {RoutedProps, withRouter} from "../HOC/withRouter";
import {Dispatch} from "redux";
import {NavigateFunction} from "react-router";
import {AuthState, OktaAuth} from "@okta/okta-auth-js";
import {withOktaAuth} from "@okta/okta-react";

interface Props extends RoutedProps {
    dispatch: Dispatch,
    oktaAuth: OktaAuth
    authState: AuthState
}

class Header extends PureComponent<Props> {
    constructor(props: Props) {
        super(props);
    }

    login = async () => {
        const {oktaAuth,} = this.props;

        await oktaAuth.signInWithRedirect();
    }

     logout = async () => {
        const {oktaAuth } = this.props;

        await oktaAuth.signOut();
    }

    render() {
        const {authState} = this.props;

        return (<div className='h-20'>
                <div
                    className={`flex ${authState?.isAuthenticated ? 'justify-between' : 'justify-end'} bg-white items-center border-b-2 border-gray-100 h-20 md:justify-start md:space-x-10 px-4 fixed w-full z-10`}>
                    {
                        authState?.isAuthenticated ?
                            <>
                                <Link to='/home'>
                                    Home
                                </Link>
                                <div className="md:flex items-center justify-end md:flex-1 lg:w-0">
                                    <button
                                        className="ml-8 whitespace-nowrap inline-flex items-center justify-center px-4 py-2 border border-transparent rounded-md shadow-sm text-base font-medium text-white bg-indigo-600 hover:bg-indigo-700"
                                        onClick={this.logout}
                                    >
                                        Sign Out
                                    </button>
                                </div>
                            </>
                            :
                            <div className=" md:flex items-center justify-end md:flex-1 lg:w-0">
                                <button
                                    onClick={this.login}
                                    className="ml-8 whitespace-nowrap inline-flex items-center justify-center px-4 py-2 border border-transparent rounded-md shadow-sm text-base font-medium text-white bg-indigo-600 hover:bg-indigo-700"
                                >
                                    Sign in
                                </button>
                            </div>
                    }
                </div>
            </div>
        )
    }
}

export default connect()(withOktaAuth(Header));
