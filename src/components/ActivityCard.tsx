import {PureComponent} from "react";
import ToDo from "./ToDo";
import {PencilIcon, XIcon} from '@heroicons/react/solid'
import {Activity} from "../Pages/NewActivity";
import {RoutedProps, withRouter} from "../HOC/withRouter";
import {connect} from "react-redux";
import {Dispatch} from "redux";
import {onEditActivity} from "../actions/activityActions";
import {ActivityState} from "../reducers/activityReducer";
import {AuthState} from "@okta/okta-auth-js";
import {withOktaAuth} from "@okta/okta-react";

interface Props extends Activity {
    onDelete: Function,
    dispatch: Dispatch,
    activity: ActivityState,
    authState?: AuthState,
}

class ActivityCard extends PureComponent<Props & RoutedProps> {
    _renderToDos = () => {
        const {toDoList,} = this.props

        return toDoList.map(toDo => <ToDo
            key={toDo.id}
            id={toDo.id.toString()}
            text={toDo.name}
            name={toDo.name}
            status={toDo.status}
            onChangeStatus={this._onChangeToDoStatus}
        />)
    }

    _handleNavigate = () => {
        const {id, navigate} = this.props;

        if (navigate) {
            navigate(`/activity/${id}`);
        }
    }

    _onChangeActivityStatus = () => {
        const {id, status, authState, activity, dispatch,} = this.props;
        const {activityArray,} = activity;
        // @ts-ignore
        const {accessToken,} = authState.accessToken;
        const editedActivity = activityArray.find(activity => activity.id === id);
        const newStatus = status === 'Done' ? 'Not Done' : 'Done'
        const newToDoList = editedActivity.toDoList.map((toDo: any) => ({
            ...toDo,
            status: newStatus,
        }))

        const data = {
            ...editedActivity,
            status: newStatus,
            toDoList: newToDoList,
        }

        dispatch(onEditActivity(data, accessToken) as any);
    }

    _onChangeToDoStatus = (toDoId: string) => () => {
        const {id, activity, authState, dispatch,} = this.props;
        const {activityArray,} = activity;
        // @ts-ignore
        const {accessToken,} = authState.accessToken;
        const editedActivity = activityArray.find(activity => activity.id === id);
        const newToDoList: any[] = [];
        let toDoIndex = null;
        let nrOfCompletedToDos = 0;
        let newActivityStatus = 'Not Done';

        //creating copy of toDoList
        editedActivity.toDoList.forEach((toDo: any, index: number) => {
            newToDoList.push(toDo);

            if (toDo?.id.toString() === toDoId) {
                toDoIndex = index;
            }

            if (toDo.id.toString() !== toDoId && toDo.status === 'Done' || toDo.id.toString() === toDoId && toDo.status === 'Not Done') {
                nrOfCompletedToDos++;
            }
        })

        if (toDoIndex !== null) {
            const newToDoStatus = newToDoList[toDoIndex].status === 'Done' ? 'Not Done' : 'Done'

            newToDoList[toDoIndex] = {
                ...newToDoList[toDoIndex],
                status: newToDoStatus
            };
        }

        if (nrOfCompletedToDos === editedActivity.toDoList.length) {
            newActivityStatus = 'Done';
        }

        const data = {
            ...editedActivity,
            status: newActivityStatus,
            toDoList: newToDoList,
        }

        dispatch(onEditActivity(data, accessToken) as any);
    }

    render() {
        const {id, title, description, toDoList, status, onDelete} = this.props

        return (
            <div className="bg-white shadow overflow-hidden sm:rounded-md md:w-1/2 w-full mb-8">
                <div className="bg-white px-4 py-5 sm:px-6">
                    <div className="-ml-4 -mt-4 mb-5 flex justify-between items-center flex-wrap sm:flex-nowrap">
                        <div className="flex flex-col-reverse md:flex-row ml-4 mt-4 justify-between w-full">
                            <h3 className="text-lg leading-6 font-medium text-gray-900 mb-5">{title}</h3>
                            <div className='flex md:justify-end mb-4 items-center'>
                                <p
                                    className={`w-24 px-5 py-1 inline-flex text-xs leading-5 font-semibold justify-center rounded-full ${status === 'Not Done' ? 'bg-red-300 text-red-800' : 'bg-green-100 text-green-800'}  mr-4 cursor-pointer`}
                                    onClick={this._onChangeActivityStatus}
                                >
                                    {status}
                                </p>
                                <PencilIcon
                                    className="h-6 w-6 text-gray-400 mr-4 cursor-pointer"
                                    onClick={this._handleNavigate}
                                />
                                <XIcon
                                    className="h-6 w-6 text-gray-400 cursor-pointer"
                                    onClick={onDelete(id)}
                                />
                            </div>
                        </div>
                    </div>
                    <p className="mt-1 mb-5 text-sm text-gray-500">
                        {description}
                    </p>
                    {
                        !!toDoList.length && <>
							<h3 className="mb-5 text-lg leading-6 font-medium text-gray-900">Todo List</h3>
							<fieldset className="space-y-5">
                                {this._renderToDos()}
							</fieldset>
						</>
                    }
                </div>
            </div>
        )
    }
}

interface EditActivityReduxState {
    activity: ActivityState,
}

const mapStateToProps = (state: EditActivityReduxState) => ({
    activity: state.activity,
})

export default connect(mapStateToProps)(withRouter(withOktaAuth(ActivityCard as any)));
