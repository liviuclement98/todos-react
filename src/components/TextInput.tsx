import React, {ChangeEventHandler, FocusEventHandler, MouseEventHandler, PureComponent} from "react";
import {XIcon} from "@heroicons/react/solid";

interface InputProps {
    type?: string,
    name: string,
    id: string,
    label?: string,
    placeholder?: string,
    error?: boolean,
    onChange: ChangeEventHandler,
    value: string,
    onBlur: FocusEventHandler,
    errorMessage?: string,
    deleteButton?: boolean,
    onDelete?: MouseEventHandler,
    inputWidth?: string,
}

class TextInput extends PureComponent<InputProps> {
    render() {
        const {type, name, id, label, placeholder, onChange, value, error,errorMessage,deleteButton, onDelete, onBlur, inputWidth} = this.props;

        return <div className='mb-6'>
            {!!label &&
			<label htmlFor="email" className="block text-sm font-medium text-gray-700">
                {label}
			</label>
            }
            <div className="mt-1 flex items-center">
                <input
                    type={type || 'text'}
                    name={name}
                    value={value}
                    id={id}
                    className={`${inputWidth || 'w-full'} shadow-sm block  sm:text-sm rounded-md ${error ? 'red-300 text-red-900 placeholder-red-300 focus:outline-none focus:ring-red-500 focus:border-red-500' : 'focus:ring-indigo-500 focus:border-indigo-500 border-gray-300'}`}
                    placeholder={placeholder}
                    onChange={onChange}
                    onBlur={onBlur}
                />
                {
                    deleteButton &&
                    <XIcon
	                    className="h-6 w-6 text-gray-400 cursor-pointer ml-4"
	                    onClick={onDelete}
                    />
                }
            </div>
            {error &&
			<p className="mt-2 text-sm text-red-600" id="email-error">
                {errorMessage}
			</p>
            }
        </div>
    }
}

export default TextInput
