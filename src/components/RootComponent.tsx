import React, {PureComponent} from "react";
import Header from "./Header";
import {Route, Routes} from "react-router-dom";
import RequireLoggedIn from "../HOC/RequireLoggedIn";
import Home from "../Pages/Home";
import NewActivity from "../Pages/NewActivity";
import EditActivity from "../Pages/EditActivity";
import RequireLoggedOut from "../HOC/RequireLoggedOut";
import {LoginCallback, withOktaAuth} from "@okta/okta-react";
import ErrorPage from "../Pages/ErrorPage";
import {AuthState} from "@okta/okta-auth-js";
import {getAllActivities} from "../actions/activityActions";
import {connect} from 'react-redux';
import {Dispatch} from "redux";
import LandingPage from "../Pages/LandingPage";

interface Props {
    authState: AuthState,
    dispatch: Dispatch,
}

class RootComponent extends PureComponent<Props> {
    componentDidUpdate(prevProps: Readonly<Props>, prevState: Readonly<{}>, snapshot?: any) {
        const {authState, dispatch} = this.props;
        // @ts-ignore
        const {accessToken} = authState;

        if(authState?.isAuthenticated && !prevProps?.authState?.isAuthenticated) {
            dispatch(getAllActivities(accessToken?.accessToken) as any)
        }
    }

    render() {
        return <>
            <Header/>
            <Routes>
                <Route
                    path="/home"
                    element={
                        <RequireLoggedIn>
                            <Home/>
                        </RequireLoggedIn>
                    }
                />
                <Route
                    path="/activity/new"
                    element={
                        <RequireLoggedIn>
                            <NewActivity/>
                        </RequireLoggedIn>}
                />
                <Route
                    path="/activity/:id"
                    element={
                        <RequireLoggedIn>
                            <EditActivity/>
                        </RequireLoggedIn>}
                />
                <Route
                    path="/"
                    element={
                        <RequireLoggedOut>
                            <LandingPage/>
                        </RequireLoggedOut>}
                />
                <Route
                    path="/login/callback"
                    element={<LoginCallback/>}
                />
                <Route
                    path='*'
                    element={<ErrorPage/>}
                />
            </Routes>
        </>;
    }
}

export default connect()(withOktaAuth(RootComponent as any));
