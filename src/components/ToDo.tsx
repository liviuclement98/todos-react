import {ChangeEventHandler, PureComponent} from "react";

interface Props {
    text: string,
    name: string,
    id: string,
    status: string,
    onChangeStatus: Function
}

class ToDo extends PureComponent<Props> {
    render() {
        const {text, name, id, status, onChangeStatus} = this.props;

        return <div className="relative flex items-start">
            <div className="flex items-center h-5 ">
                <input
                    id={`${name}-${id}`}
                    aria-describedby="comments-description"
                    name={name}
                    type="checkbox"
                    className="focus:ring-indigo-500 h-4 w-4 text-indigo-600 border-gray-300 rounded"
                    checked={status === "Done"}
                    onChange={onChangeStatus(id)}
                />
            </div>
            <div className="ml-3 text-sm">
                <label htmlFor={`${name}-${id}`} className="font-medium text-gray-500">
                    {text}
                </label>
            </div>
        </div>
    }
}

export default ToDo;
