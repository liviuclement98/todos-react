import {calls,} from '../utils/calls.js';
import {API_URL,} from '../env';
import {Dispatch} from "redux";
import axios from 'axios';

/**
 * Login a user
 * @param data {Object}
 * @param successCallback {Function}
 * @returns {Function}
 */
export const onLogin = (data: any, successCallback: any) => {
    return (dispatch: any) => {
        dispatch({type: 'ON_LOGIN',});
        axios(calls.onLogin(API_URL, data))
            .then((res: any) => {
                dispatch({
                    type: 'ON_LOGIN_FULFILLED',
                });
                if (successCallback) {
                    successCallback({
                        auth_token: res.auth_token,
                    });
                }
            })
            .catch((err: any) => {
                dispatch({
                    type: 'ON_LOGIN_REJECTED',
                    payload: {error: err,},
                });
            });
    };
};

/**
 * Validate a user's token
 * @param token {String}
 * @param dispatch {Function}
 * @param language {String}
 * @returns {Promise}
 */
export const validateToken = (token: any, dispatch: any, language: any) => {
    return new Promise(resolve => {
        let response;
        resolve({
            valid: true,
            id: 23,
        });

    });
};
