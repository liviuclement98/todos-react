import {calls,} from '../utils/calls';
import {API_URL,} from '../env';
import axios from 'axios';

export const getAllActivities = (accessToken: any, successCallback?: Function) => {
    return (dispatch: any) => {
        dispatch({type: 'ON_GET_ALL_ACTIVITIES',});
        axios(calls.getAllActivities(API_URL, accessToken))
            .then((res) => {
                dispatch({
                    type: 'ON_GET_ALL_ACTIVITIES_FULFILLED',
                    payload: {activityArray: res.data.activities},
                });
                if (successCallback) {
                    successCallback();
                }
            })
            .catch((err: any) => {
                dispatch({
                    type: 'ON_GET_ALL_ACTIVITIES_REJECTED',
                    payload: {error: err.message,},
                });
            });
    };
};

export const onAddNewActivity = (data: any, accessToken: any, successCallback?: Function) => {
    return (dispatch: any) => {
        dispatch({type: 'ON_ADD_NEW_ACTIVITY',});
        axios(calls.addNewActivity(API_URL, data, accessToken))
            .then((res) => {
                dispatch({
                    type: 'ON_ADD_NEW_ACTIVITY_FULFILLED',
                    payload: {newActivity: res.data.newActivity},
                });
                if (successCallback) {
                    successCallback();
                }
            })
            .catch((err: any) => {
                dispatch({
                    type: 'ON_ADD_NEW_ACTIVITY_REJECTED',
                    payload: {error: err.message,},
                });
            });
    };
};

export const onEditActivity = (data: any, accessToken: any, successCallback?: Function) => {
    return (dispatch: any) => {
        console.log({accessToken})
        dispatch({type: 'ON_EDIT_ACTIVITY',});
        axios(calls.editActivity(API_URL, data.id, data, accessToken))
            .then(res => {
                dispatch({
                    type: 'ON_EDIT_ACTIVITY_FULFILLED',
                    payload: {editedActivity: res.data.editedActivity},
                });
                if (successCallback) {
                    successCallback();
                }
            })
            .catch((err: any) => {
                dispatch({
                    type: 'ON_EDIT_ACTIVITY_REJECTED',
                    payload: {error: err.message,},
                });
            });
    };
};

export const onDeleteActivity = (activityId: any, accessToken: any, successCallback?: Function) => {
    return (dispatch: any) => {
        dispatch({type: 'ON_DELETE_ACTIVITY',});
        axios(calls.deleteActivity(API_URL, activityId,accessToken))
            .then(() => {
                dispatch({
                    type: 'ON_DELETE_ACTIVITY_FULFILLED',
                    payload: {deletedActivityId: activityId},
                });

                if (successCallback) {
                    successCallback();
                }
            })
            .catch((err: any) => {
                dispatch({
                    type: 'ON_DELETE_ACTIVITY_REJECTED',
                    payload: {error: err.message,},
                });
            });
    };
};
