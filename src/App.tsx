import React, {PureComponent} from 'react';
import {connect} from 'react-redux'
import {BrowserRouter as Router} from 'react-router-dom';
import {AuthenticationState} from "./reducers/authenticationReducer";
import {Dispatch} from "redux";
import {OktaAuth, toRelativeUrl} from '@okta/okta-auth-js';
import {Security} from '@okta/okta-react';
import {API_URL, oktaClientId, oktaDomain} from './env'
import {withRouter} from "./HOC/withRouter";
import axios from "axios";
import {calls} from "./utils/calls";
import RootComponent from "./components/RootComponent";

const oktaAuth = new OktaAuth({
    issuer: `https://${oktaDomain}/oauth2/default`,
    clientId: oktaClientId,
    redirectUri: `${window.location.origin}/login/callback`
})

interface Props {
    dispatch: Dispatch
    isLoggedIn: boolean;
    navigate?: any;
}

class App extends PureComponent<Props> {
    restoreOriginalUri = async (_oktaAuth: any, originalUri: any) => {
        const {navigate} = this.props;
        const {sub, uid} = _oktaAuth.authStateManager._authState.accessToken.claims;
        const data = {
            email: sub,
            oktaId: uid,
        }

        await axios(calls.onLogin(API_URL, data));

        navigate(toRelativeUrl(originalUri || '/home', window.location.origin), {replace: true});
    }

    render() {
        return <Security oktaAuth={oktaAuth} restoreOriginalUri={this.restoreOriginalUri}>
            <RootComponent/>
        </Security>
    }
}

const AppWithRouterAccess = withRouter(App as any);

class RouterApp extends PureComponent {
    render() {
        return (<Router><AppWithRouterAccess/></Router>);
    }
}

interface AppReduxState {
    authentication: AuthenticationState
}

const mapStateToProps = (state: AppReduxState) => ({
    isLoggedIn: state.authentication.isLoggedIn,
})

export default connect(mapStateToProps)(RouterApp);
