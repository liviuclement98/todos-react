/**
 * Regular JSON Headers - no authentication
 * @param language {String}
 * @returns {{'Content-type': {String}, Accept: {String}, 'Content-Language': {String}}}
 */
const getJSONHeaders = (language: string = 'en') => ( {
	'Content-type': 'application/json',
	'Accept': 'application/json',
	'Content-Language': language,
} );

/**
 * JSON Header with authentication (if provided), otherwise just JSON Headers and the request will probably throw 401
 * @param token {String}
 * @param language {String}
 * @returns {{Authorization: {String}, 'Content-type': {String}, Accept: {String}, 'Content-Language': {String}}|{'Content-type': {String}, Accept: {String}, 'Content-Language': {String}}}
 */
const getAuthenticatedHeaders = (token: string | null = null, language: string = 'en') => {
	if (!token) {
		return {
			'Content-type': 'application/json',
			'Accept': 'application/json',
			'Content-Language': language,
		};
	}
	return {
		'Content-type': 'application/json',
		'Accept': 'application/json',
		'Content-Language': language,
		'Authorization': `Bearer ${ token }`,
	};
};

export const calls = {
	onLogin: (service: string, data: any): any => ( {
		url: `${ service }/api/users/authenticate`,
		method: 'POST',
		headers: getJSONHeaders(),
		data: data,
	} ),
	getAllActivities: (service: string, accessToken: string): any => ( {
		url: `${ service }/api/activities`,
		method: 'GET',
		headers: getAuthenticatedHeaders(accessToken),
	} ),
	addNewActivity: (service: string, data: any, accessToken: string): any => ( {
		url: `${ service }/api/activities`,
		method: 'POST',
		headers: getAuthenticatedHeaders(accessToken),
		data: data,
	} ),
	editActivity: (service: string, activityId: any, data: any, accessToken: string): any => ( {
		url: `${ service }/api/activities/${activityId}`,
		method: 'PUT',
		headers: getAuthenticatedHeaders(accessToken),
		data: data,
	} ),
	deleteActivity: (service: string, activityId: any, accessToken: string): any => ( {
		url: `${ service }/api/activities/${activityId}`,
		method: 'DELETE',
		headers: getAuthenticatedHeaders(accessToken),
	} ),
};
