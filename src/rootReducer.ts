import { combineReducers, } from 'redux';
import authenticationReducer from './reducers/authenticationReducer';
import activityReducer from "./reducers/activityReducer";

export const createRootReducer = () => combineReducers({
    authentication: authenticationReducer,
    activity: activityReducer,
});
