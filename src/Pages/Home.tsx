import React, {PureComponent} from "react";
import ActivityCard from "../components/ActivityCard";
import {Link} from "react-router-dom";
import {connect} from 'react-redux'
import {ActivityState} from "../reducers/activityReducer";
import {Dispatch} from "redux";
import {onDeleteActivity} from "../actions/activityActions";
import ComponentWrapper from "../HOC/ComponentWrapper";
import {withOktaAuth} from "@okta/okta-react";
import {AuthState} from "@okta/okta-auth-js";

interface Props {
    dispatch: Dispatch,
    activity: ActivityState,
    authState: AuthState,
}

class Home extends PureComponent<Props> {
    constructor(props: any) {
        super(props);
    }

    _deleteActivity = (id: string) => () => {
        const {dispatch, authState} = this.props;
        // @ts-ignore
        const {accessToken} = authState.accessToken;

        dispatch(onDeleteActivity(id, accessToken) as any);
    }

    _renderCards = () => {
        const {activity} = this.props;
        const {activityArray} = activity;

        return activityArray.map(activity => {
            return (<ActivityCard
                key={activity.id}
                // @ts-ignore
                id={activity.id}
                title={activity.title}
                description={activity.description}
                toDoList={activity.toDoList}
                status={activity.status}
                onDelete={this._deleteActivity}
            />)
        })
    }

    render() {
        const {activity} = this.props;
        const {deletingActivityError, editingActivityError} = activity;

        return <ComponentWrapper
            errorMessage={deletingActivityError || editingActivityError}
        >
            <div
                className='flex flex-col items-center w-full pt-12 relative mb-16'
            >
                {this._renderCards()}
                <div
                    className='w-full flex sticky bottom-16 justify-center'
                >
                    <Link
                        className={`cursor-pointer md:w-1/4 w-5/6 whitespace-nowrap inline-flex items-center justify-center px-4 py-2 border border-transparent rounded-md shadow-sm text-base font-medium text-white bg-indigo-600 hover:bg-indigo-700`}
                        to='/activity/new'
                    >
                        Add Activity
                    </Link>
                </div>
            </div>
        </ComponentWrapper>
    }
}

interface HomeReduxState {
    activity: ActivityState
}

const mapStateToProps = (state: HomeReduxState) => ({
    activity: state.activity
})

export default connect(mapStateToProps)(withOktaAuth(Home as any))
