import React, {ChangeEvent, ChangeEventHandler, PureComponent} from "react";
import TextInput from "../components/TextInput";
import {FormikProps, withFormik} from "formik";
import * as Yup from "yup";
import {Dispatch} from "redux";
import {onAddNewActivity} from "../actions/activityActions";
import {connect} from "react-redux";
import {RoutedProps, withRouter} from "../HOC/withRouter";
import ComponentWrapper from '../HOC/ComponentWrapper';
import {ActivityState} from "../reducers/activityReducer";
import {withOktaAuth} from "@okta/okta-react";
import {AuthState} from "@okta/okta-auth-js";

interface ToDo {
    id: number,
    name: string,
    value: string,
    new: boolean,
    status: string,
}

interface Props extends RoutedProps {
    dispatch: Dispatch,
    activity: ActivityState,
    authState: AuthState,
}

class NewActivity extends PureComponent<Props & FormikProps<Activity>, any> {
    async componentDidMount() {
        const {validateForm} = this.props;

        await validateForm();
    }

    _onSaveActivity = () => {
        const {errors, values, authState, dispatch} = this.props;
        const canAddActivity = !Object.values(errors).length;
        // @ts-ignore
        const {accessToken = ''} = authState.accessToken;

        if (!canAddActivity) {
            return;
        }

        const {toDoList} = values;
        const cleanToDoList = toDoList.map(toDo => ({
            name: toDo.value,
            status: toDo.status,
        }));
        cleanToDoList.pop();

        const data = {
            ...values,
            toDoList: cleanToDoList,
            id: Math.random(),
            status: 'Not Done'
        }

        dispatch(onAddNewActivity(data, accessToken, this._onSaveActivitySuccessCallback) as any)
    }

    _onSaveActivitySuccessCallback = () => {
        const {navigate} = this.props;

        if (navigate) {
            navigate('/home');
        }
    }

    _onToDoChange = (index: number) => (event: ChangeEvent) => {
        const {values, setFieldValue} = this.props;
        const {toDoList} = values;
        const element = event.currentTarget as HTMLInputElement;
        const toDoListLength = toDoList.length;
        const newToDoList = [...toDoList];


        if (index === toDoListLength - 1) {
            newToDoList.push({
                id: toDoListLength + 1,
                name: `todo${toDoListLength + 1}`,
                value: '',
                new: true,
                status: 'Not Done'
            });
        }

        newToDoList[index] = {...newToDoList[index], value: element.value, new: false}

        setFieldValue('toDoList', newToDoList);
    }

    _onDeleteToDo = (index: number) => () => {
        const {values, setFieldValue} = this.props;
        const {toDoList} = values;
        const newToDoList = toDoList.filter((toDo, toDoIndex) => index !== toDoIndex);

        setFieldValue('toDoList', newToDoList);
    }

    _renderToDoList = () => {
        const {values, errors, touched, handleBlur,} = this.props;
        const {toDoList} = values;

        return toDoList.map((toDo: ToDo, index: number) => <TextInput
                inputWidth='w-11/12'
                key={`${toDo}-${index}`}
                name={toDo.name}
                label={`TODO #${index + 1}`}
                placeholder={`TODO #${index + 1}`}
                id={`${toDo.name}-${toDo.id}`}
                onChange={this._onToDoChange(index)}
                value={toDo.value}
                onBlur={handleBlur(`toDoList[${index}]`)}
                error={!!errors.toDoList?.[index] && !!touched.toDoList?.[index]}
                // @ts-ignore
                errorMessage={errors.toDoList?.[index]?.value}
                deleteButton={!toDo.new}
                onDelete={this._onDeleteToDo(index)}
            />
        );
    }

    render() {
        const {values, touched, errors,activity, handleBlur, handleChange,} = this.props;
        const {title, description,} = values;
        const {addingNewActivityError,} = activity;
        const canAddActivity = !Object.values(errors).length;

        return <ComponentWrapper
            errorMessage={addingNewActivityError}
        >
            <div
                className='flex flex-col items-center w-full pt-12 relative min-h-full justify-between'
            >
                <div className="bg-white shadow overflow-hidden sm:rounded-md md:w-1/2 mb-8">
                    <div className="bg-white px-4 py-5 sm:px-6">
                        <TextInput
                            name='title'
                            id='title'
                            label='Title'
                            placeholder='Title'
                            onChange={handleChange('title')}
                            value={title}
                            onBlur={handleBlur('title')}
                            error={!!errors.title && !!touched.title}
                            errorMessage={errors.title}
                        />
                        <TextInput
                            name='description'
                            id='description'
                            label='Description'
                            placeholder='Description'
                            onChange={handleChange('description')}
                            value={description}
                            onBlur={handleBlur('description')}
                            error={!!errors.description && !!touched.description}
                            errorMessage={errors.description}
                        />
                        <h3 className="mb-5 text-lg leading-6 font-medium text-gray-700">Todo List</h3>
                        {this._renderToDoList()}
                    </div>
                </div>
                <button
                    className={`mb-20 cursor-pointer w-1/4 whitespace-nowrap inline-flex items-center justify-center px-4 py-2 border border-transparent rounded-md shadow-sm text-base font-medium text-white ${canAddActivity ? 'bg-indigo-600 hover:bg-indigo-700' : 'bg-gray-300'}`}
                    onClick={this._onSaveActivity}
                >
                    Save
                </button>
            </div>
        </ComponentWrapper>
    }
}


export interface Activity {
    id?: string,
    title: string,
    description: string,
    toDoList: Array<ToDo>,
    status?: string,
}

const FormikNewActivity = withFormik<Props, Activity>({
    mapPropsToValues: () => ({
        title: '',
        description: '',
        toDoList: [{
            id: 1,
            name: 'todo1',
            value: '',
            new: true,
            status: 'Not Done'
        }],
        status: 'Not Done',
    }),
    validationSchema: () => {
        return Yup.object({
            title: Yup.string().min(2, 'Titlul trebuie sa contina minim 2 caractere').required('Acest camp este obligatoriu'),
            description: Yup.string().min(8, 'Descrierea trebuie sa contina minim 8 caractere').required('Acest camp este obligatoriu'),
            toDoList: Yup.array().of(
                Yup.object({
                    id: Yup.number(),
                    name: Yup.string(),
                    value: Yup.string().when('new', {
                        is: true,
                        then: Yup.string().min(4, 'TODO-ul trebuie sa contina minim 4 caractere'),
                        otherwise: Yup.string(),
                    }),
                    new: Yup.boolean(),
                })
            )
        });
    },
    handleSubmit: () => {
    },
})(NewActivity);


interface EditActivityReduxState {
    activity: ActivityState,
}

const mapStateToProps = (state: EditActivityReduxState) => ({
    activity: state.activity,
})

export default connect(mapStateToProps)(withRouter(withOktaAuth(FormikNewActivity as any)));
