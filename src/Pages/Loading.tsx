import {PureComponent} from "react";

class Loading extends PureComponent{
    render() {
        return <p>
            loading...
        </p>;
    }
}

export default Loading;
