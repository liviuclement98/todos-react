//basic class react component
import React from "react";
import {AuthenticationState} from "../reducers/authenticationReducer";
import {FormikProps, withFormik} from "formik";
import * as Yup from "yup";
import {connect} from "react-redux";
import {Dispatch} from "redux";
import {RoutedProps, withRouter} from "../HOC/withRouter";

interface FormValues {
    email: string;
    password: string;
    confirmPassword: string;
}

interface Props extends RoutedProps {
    authentication?: AuthenticationState,
    dispatch?: Dispatch
}

interface State {

}

class SignUp extends React.PureComponent<Props & FormikProps<FormValues>, State> {
    _onSubmit = () => {
        const {values, dispatch} = this.props;
        const data = {
            email: values.email,
            password: values.password
        };
    };

    render() {
        const { values, errors, touched, isSubmitting, handleChange, handleBlur, handleSubmit } = this.props;

        return (
            <>
                <div className="min-h-full flex flex-col justify-center py-12 sm:px-6 lg:px-8">
                    <div className="sm:mx-auto sm:w-full sm:max-w-md">
                        <h2 className="mt-6 text-center text-3xl font-extrabold text-gray-900">Create an account</h2>
                    </div>

                    <div className="mt-8 sm:mx-auto sm:w-full sm:max-w-md">
                        <div className="bg-white py-8 px-4 shadow sm:rounded-lg sm:px-10 space-y-6">
                            <div>
                                <label htmlFor="email" className="block text-sm font-medium text-gray-700">
                                    Email address
                                </label>
                                <div className="mt-1">
                                    <input
                                        id="email"
                                        name="email"
                                        type="email"
                                        autoComplete="email"
                                        required
                                        className="appearance-none block w-full px-3 py-2 border border-gray-300 rounded-md shadow-sm placeholder-gray-400 focus:outline-none focus:ring-indigo-500 focus:border-indigo-500 sm:text-sm"
                                        onChange={handleChange('email')}
                                        onBlur={handleBlur('email')}
                                        value={values.email}
                                    />
                                </div>
                                {errors.email && touched.email && (
                                    <p className="mt-2 text-sm text-red-600">{errors.email}</p>
                                )}
                            </div>

                            <div>
                                <label htmlFor="password" className="block text-sm font-medium text-gray-700">
                                    Password
                                </label>
                                <div className="mt-1">
                                    <input
                                        id="password"
                                        name="password"
                                        type="password"
                                        autoComplete="current-password"
                                        required
                                        className="appearance-none block w-full px-3 py-2 border border-gray-300 rounded-md shadow-sm placeholder-gray-400 focus:outline-none focus:ring-indigo-500 focus:border-indigo-500 sm:text-sm"
                                        onChange={handleChange('password')}
                                        onBlur={handleBlur('password')}
                                        value={values.password}
                                    />
                                </div>
                                {errors.password && touched.password && (
                                    <p className="mt-2 text-sm text-red-600">{errors.password}</p>
                                )}
                            </div>

                            <div>
                                <label htmlFor="confirmPassword" className="block text-sm font-medium text-gray-700">
                                    Confirm Password
                                </label>
                                <div className="mt-1">
                                    <input
                                        id="confirmPassword"
                                        name="confirmPassword"
                                        type="password"
                                        // autoComplete="current-password"
                                        required
                                        className="appearance-none block w-full px-3 py-2 border border-gray-300 rounded-md shadow-sm placeholder-gray-400 focus:outline-none focus:ring-indigo-500 focus:border-indigo-500 sm:text-sm"
                                        onChange={handleChange('confirmPassword')}
                                        onBlur={handleBlur('confirmPassword')}
                                        value={values.confirmPassword}
                                    />
                                </div>
                                {errors.confirmPassword && touched.confirmPassword && (
                                    <p className="mt-2 text-sm text-red-600">{errors.confirmPassword}</p>
                                )}
                            </div>

                            <div className="flex items-center justify-between">
                                <div className="text-sm">
                                    <a href="#" className="font-medium text-indigo-600 hover:text-indigo-500">
                                        Forgot your password?
                                    </a>
                                </div>
                            </div>

                            <div>
                                <button
                                    type="submit"
                                    className="w-full flex justify-center py-2 px-4 border border-transparent rounded-md shadow-sm text-sm font-medium text-white bg-indigo-600 hover:bg-indigo-700 focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-indigo-500"
                                >
                                    Sign in
                                </button>
                            </div>
                        </div>
                    </div>
                </div>
            </>
        );
    }
}

const FormikSignUp = withFormik<Props, FormValues>({
    mapPropsToValues: () => ( {
        email: 'liviuclement98@gmail.com' ,
        password: '12345678',
        confirmPassword: '12345678',
    } ),
    validationSchema: () => {
        return Yup.object({
            email: Yup.string().email('Emailul nu este valid').required('Acest camp este obligatoriu'),
            password: Yup.string().min(8, 'Parola trebuie sa contina minim 8 caractere').required('Acest camp este obligatoriu'),
            confirmPassword: Yup.string().min(8, 'Parola trebuie sa contina minim 8 caractere').required('Acest camp este obligatoriu').when('password', {
                is: (val: string) => ( val && val.length > 0 ),
                then: Yup.string().oneOf(
                    [Yup.ref('password')],
                    'Parolele nu se potrivesc'
                ),
            }),
        });
    },
    handleSubmit: () => {},
})(SignUp);

interface AppReduxState {
    authentication: AuthenticationState
}

const mapStateToProps = (state: AppReduxState) => ({
    authentication: state.authentication,
})

export default connect(mapStateToProps)(withRouter(FormikSignUp));
