import React, {ChangeEvent, ChangeEventHandler, PureComponent} from "react";
import TextInput from "../components/TextInput";
import {FormikProps, withFormik} from "formik";
import * as Yup from "yup";
import {Dispatch} from "redux";
import {onEditActivity} from "../actions/activityActions";
import {connect} from "react-redux";
import {RoutedProps, withRouter} from "../HOC/withRouter";
import {ActivityState} from "../reducers/activityReducer";
import {v4 as uuidv4, validate} from 'uuid';
import ComponentWrapper from "../HOC/ComponentWrapper";
import {withOktaAuth} from "@okta/okta-react";
import {AuthState} from "@okta/okta-auth-js";

interface ToDo {
    id: string | number,
    name: string,
    value: string,
    new: boolean,
    status: string,
}

interface Props extends RoutedProps {
    dispatch: Dispatch,
    activity: ActivityState,
    authState: AuthState,
}

class EditActivity extends PureComponent<Props & FormikProps<Activity>, any> {
    async componentDidMount() {
        const {validateForm} = this.props;

        await validateForm();
    }

    _onSaveActivity = () => {
        const {errors, values,authState, dispatch} = this.props;
        const canAddActivity = !Object.values(errors).length;
        // @ts-ignore
        const {accessToken,} = authState.accessToken;

        if (!canAddActivity) {
            return;
        }

        const {toDoList} = values;
        const cleanToDoList = toDoList.map(toDo => {
            if (validate(toDo.id as string)) {
                return ({
                    name: toDo.value,
                    status: toDo.status,
                })
            }

            return ({
                id: toDo.id,
                name: toDo.value,
                status: toDo.status,
            })
        });

        cleanToDoList.pop();

        const data = {
            ...values,
            toDoList: cleanToDoList,
            status: values.status === 'Done' ? values.initialToDoListLength < cleanToDoList.length ? 'Not Done' : 'Done' : 'Not Done',
        }

        dispatch(onEditActivity(data, accessToken, this._onEditActivitySuccessCallback) as any)
    }

    _onEditActivitySuccessCallback = () => {
        const {navigate} = this.props;

        if (navigate) {
            navigate('/home');
        }
    }

    _onToDoChange = (index: number) => (event: ChangeEvent) => {
        const {values, setFieldValue} = this.props;
        const {toDoList} = values;
        const element = event.currentTarget as HTMLInputElement;
        const toDoListLength = toDoList.length;
        const newToDoList = [...toDoList];

        if (index === toDoListLength - 1) {
            newToDoList.push({
                id: uuidv4(),
                name: `todo${toDoListLength + 1}`,
                value: '',
                new: true,
                status: 'Not Done',
            });
        }

        newToDoList[index] = {...newToDoList[index], value: element.value, new: false}

        setFieldValue('toDoList', newToDoList);
    }

    _onDeleteToDo = (index: number) => () => {
        const {values, setFieldValue} = this.props;
        const {toDoList} = values;

        const newToDoList = toDoList.filter((toDo, toDoIndex) => index !== toDoIndex);

        setFieldValue('toDoList', newToDoList);
    }

    _renderToDoList = () => {
        const {values, errors, touched, handleBlur,} = this.props;
        const {toDoList} = values;

        return toDoList.map((toDo: ToDo, index: number) => <TextInput
                inputWidth='w-11/12'
                key={`${toDo}-${index}`}
                name={toDo.name}
                label={`TODO #${index + 1}`}
                placeholder={`TODO #${index + 1}`}
                id={`${toDo.name}-${toDo.id}`}
                onChange={this._onToDoChange(index)}
                value={toDo.value}
                onBlur={handleBlur(`toDoList[${index}]`)}
                error={!!errors.toDoList?.[index] && !!touched.toDoList?.[index]}
                // @ts-ignore
                errorMessage={errors.toDoList?.[index]?.value}
                deleteButton={!toDo.new}
                onDelete={this._onDeleteToDo(index)}
            />
        );
    }

    render() {
        const {values, touched, errors, activity, handleBlur, handleChange,} = this.props;
        const {title, description,} = values;
        const {editingActivityError,} = activity;
        const canAddActivity = !Object.values(errors).length;

        return <ComponentWrapper
            errorMessage={editingActivityError}
        >
            <div
                className='flex flex-col items-center w-full pt-12 relative min-h-full justify-between'
            >
                <div className="bg-white shadow overflow-hidden sm:rounded-md md:w-1/2 mb-8">
                    <div className="bg-white px-4 py-5 sm:px-6">
                        <TextInput
                            name='title'
                            id='title'
                            label='Title'
                            placeholder='Title'
                            onChange={handleChange('title')}
                            value={title}
                            onBlur={handleBlur('title')}
                            error={!!errors.title && !!touched.title}
                            errorMessage={errors.title}
                        />
                        <TextInput
                            name='description'
                            id='description'
                            label='Description'
                            placeholder='Description'
                            onChange={handleChange('description')}
                            value={description}
                            onBlur={handleBlur('description')}
                            error={!!errors.description && !!touched.description}
                            errorMessage={errors.description}
                        />
                        <h3 className="mb-5 text-lg leading-6 font-medium text-gray-700">Todo List</h3>
                        {this._renderToDoList()}
                    </div>
                </div>
                <button
                    className={`mb-20 cursor-pointer w-1/4 whitespace-nowrap inline-flex items-center justify-center px-4 py-2 border border-transparent rounded-md shadow-sm text-base font-medium text-white ${canAddActivity ? 'bg-indigo-600 hover:bg-indigo-700' : 'bg-gray-300'}`}
                    onClick={this._onSaveActivity}
                >
                    Save
                </button>
            </div>
        </ComponentWrapper>
    }
}

export interface Activity {
    id?: string,
    title: string,
    description: string,
    toDoList: Array<ToDo>,
    status?: string,
    initialToDoListLength: number,
}

const FormikEditActivity = withFormik<Props, Activity>({
    mapPropsToValues: props => {
        const {params, activity, navigate} = props;
        const {activityArray} = activity;
        const {id} = params;
        const currentActivity = activityArray.find(activity => activity.id.toString() === id)

        if (!currentActivity && navigate) {
            navigate('/home');
        }
        const uuid = uuidv4();

        return ({
            id: currentActivity?.id,
            status: currentActivity?.status,
            title: currentActivity?.title || '',
            description: currentActivity?.description || '',
            toDoList: currentActivity?.toDoList ? [...currentActivity?.toDoList.map((currentActivity: any) => ({
                ...currentActivity,
                value: currentActivity.name,
            })), {
                id: uuid,
                name: `todo1${currentActivity?.toDoList?.length}`,
                value: '',
                new: true,
                status: 'Not Done'
            }] : [{
                id: 1,
                name: 'todo1',
                value: '',
                new: true,
                status: 'Not Done'
            }],
            initialToDoListLength: currentActivity?.toDoList?.length,
        })
    },
    validationSchema: () => {
        return Yup.object({
            title: Yup.string().min(2, 'Titlul trebuie sa contina minim 2 caractere').required('Acest camp este obligatoriu'),
            description: Yup.string().min(8, 'Descrierea trebuie sa contina minim 8 caractere').required('Acest camp este obligatoriu'),
            toDoList: Yup.array().of(
                Yup.object({
                    id: Yup.string(),
                    name: Yup.string(),
                    value: Yup.string().when('new', {
                        is: false,
                        then: Yup.string().min(4, 'TODO-ul trebuie sa contina minim 4 caractere'),
                        otherwise: Yup.string(),
                    }),
                    new: Yup.boolean(),
                })
            )
        });
    },
    handleSubmit: () => {
    },
})(EditActivity);

interface EditActivityReduxState {
    activity: ActivityState,
}

const mapStateToProps = (state: EditActivityReduxState) => ({
    activity: state.activity,
})

export default connect(mapStateToProps)(withRouter(withOktaAuth(FormikEditActivity as any)));
