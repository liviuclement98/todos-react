import { applyMiddleware, createStore, compose} from 'redux';
import thunk from 'redux-thunk';
import * as rootReducer from './rootReducer';
import {Dispatch} from "redux";

let middlewares = [
    thunk
];

//@ts-ignore
const composeEnhancers = window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ || compose;

function store(){
    return createStore(rootReducer.createRootReducer(), composeEnhancers(applyMiddleware(...middlewares)));
}

export default store;
